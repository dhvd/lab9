;*******************************************************************************
;                                                                              *
;    Microchip licenses this software to you solely for use with Microchip     *
;    products. The software is owned by Microchip and/or its licensors, and is *
;    protected under applicable copyright laws.  All rights reserved.          *
;                                                                              *
;    This software and any accompanying information is for suggestion only.    *
;    It shall not be deemed to modify Microchip?s standard warranty for its    *
;    products.  It is your responsibility to ensure that this software meets   *
;    your requirements.                                                        *
;                                                                              *
;    SOFTWARE IS PROVIDED "AS IS".  MICROCHIP AND ITS LICENSORS EXPRESSLY      *
;    DISCLAIM ANY WARRANTY OF ANY KIND, WHETHER EXPRESS OR IMPLIED, INCLUDING  *
;    BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS    *
;    FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. IN NO EVENT SHALL          *
;    MICROCHIP OR ITS LICENSORS BE LIABLE FOR ANY INCIDENTAL, SPECIAL,         *
;    INDIRECT OR CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, HARM TO     *
;    YOUR EQUIPMENT, COST OF PROCUREMENT OF SUBSTITUTE GOODS, TECHNOLOGY OR    *
;    SERVICES, ANY CLAIMS BY THIRD PARTIES (INCLUDING BUT NOT LIMITED TO ANY   *
;    DEFENSE THEREOF), ANY CLAIMS FOR INDEMNITY OR CONTRIBUTION, OR OTHER      *
;    SIMILAR COSTS.                                                            *
;                                                                              *
;    To the fullest extend allowed by law, Microchip and its licensors         *
;    liability shall not exceed the amount of fee, if any, that you have paid  *
;    directly to Microchip to use this software.                               *
;                                                                              *
;    MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF    *
;    THESE TERMS.                                                              *
;                                                                              *
;*******************************************************************************
;                                                                              *
;    Filename:                                                                 *
;    Date:                                                                     *
;    File Version:                                                             *
;    Author:                                                                   *
;    Company:                                                                  *
;    Description:                                                              *
;                                                                              *
;*******************************************************************************
;                                                                              *
;    Notes: In the MPLAB X Help, refer to the MPASM Assembler documentation    *
;    for information on assembly instructions.                                 *
;                                                                              *
;*******************************************************************************
;                                                                              *
;    Known Issues: This template is designed for relocatable code.  As such,   *
;    build errors such as "Directive only allowed when generating an object    *
;    file" will result when the 'Build in Absolute Mode' checkbox is selected  *
;    in the project properties.  Designing code in absolute mode is            *
;    antiquated - use relocatable mode.                                        *
;                                                                              *
;*******************************************************************************
;                                                                              *
;    Revision History:                                                         *
;                                                                              *
;*******************************************************************************



;*******************************************************************************
; Processor Inclusion
;
; TODO Step #1 Open the task list under Window > Tasks.  Include your
; device .inc file - e.g. #include <device_name>.inc.  Available
; include files are in C:\Program Files\Microchip\MPLABX\mpasmx
; assuming the default installation path for MPLAB X.  You may manually find
; the appropriate include file for your device here and include it, or
; simply copy the include generated by the configuration bits
; generator (see Step #2).
;
;*******************************************************************************

; TODO INSERT INCLUDE CODE HERE
#include "p16f15356.inc"
    
;*******************************************************************************
;
; TODO Step #2 - Configuration Word Setup
;
; The 'CONFIG' directive is used to embed the configuration word within the
; .asm file. MPLAB X requires users to embed their configuration words
; into source code.  See the device datasheet for additional information
; on configuration word settings.  Device configuration bits descriptions
; are in C:\Program Files\Microchip\MPLABX\mpasmx\P<device_name>.inc
; (may change depending on your MPLAB X installation directory).
;
; MPLAB X has a feature which generates configuration bits source code.  Go to
; Window > PIC Memory Views > Configuration Bits.  Configure each field as
; needed and select 'Generate Source Code to Output'.  The resulting code which
; appears in the 'Output Window' > 'Config Bits Source' tab may be copied
; below.
;
;*******************************************************************************

; TODO INSERT CONFIG HERE
    __config _CONFIG1, (_RSTOSC_HFINT32 & _FEXTOSC_OFF)
    __config _CONFIG2, (_MCLRE_ON & _PWRTE_ON)
    __config _CONFIG3, (_WDTE_OFF)
    __config _CONFIG4, (H'3FFF')
    __config _CONFIG5, (_CP_OFF)

;*******************************************************************************
;
; TODO Step #3 - Variable Definitions
;
; Refer to datasheet for available data memory (RAM) organization assuming
; relocatible code organization (which is an option in project
; properties > mpasm (Global Options)).  Absolute mode generally should
; be used sparingly.
;
; Example of using GPR Uninitialized Data
;
;   GPR_VAR        UDATA
;   MYVAR1         RES        1      ; User variable linker places
;   MYVAR2         RES        1      ; User variable linker places
;   MYVAR3         RES        1      ; User variable linker places
;
;   ; Example of using Access Uninitialized Data Section (when available)
;   ; The variables for the context saving in the device datasheet may need
;   ; memory reserved here.
;   INT_VAR        UDATA_ACS
;   W_TEMP         RES        1      ; w register for context saving (ACCESS)
;   STATUS_TEMP    RES        1      ; status used for context saving
;   BSR_TEMP       RES        1      ; bank select used for ISR context saving
;
;*******************************************************************************

; VARIABLE DEFINITSIONS
Debug_Bit2          EQU     3
Debug_Bit           EQU     2
Transmit_Bit        EQU     1
Receive_Bit         EQU     0
VARIABLES           UDATA   0x20
TransmitByte        RES     1
TransmitCounter     RES     1
TransmitActive      RES     1
BSR_temp            RES     1
SendCharacter       RES     1
       
ReceiveByte         RES     1
ReceiveActive       RES     1
ReceiveStarting     RES     1
ReceiveComplete     RES     1
ReceiveCounter      RES     1

;*******************************************************************************
; Reset Vector
;*******************************************************************************
RES_VECT  CODE    0x0000            ; processor reset vector
    GOTO    START                   ; go to beginning of program

;*******************************************************************************
; TODO Step #4 - Interrupt Service Routines
;
; There are a few different ways to structure interrupt routines in the 8
; bit device families.  On PIC18's the high priority and low priority
; interrupts are located at 0x0008 and 0x0018, respectively.  On PIC16's and
; lower the interrupt is at 0x0004.  Between device families there is subtle
; variation in the both the hardware supporting the ISR (for restoring
; interrupt context) as well as the software used to restore the context
; (without corrupting the STATUS bits).
;
; General formats are shown below in relocatible format.
;
;------------------------------PIC16's and below--------------------------------
;
ISR       CODE    0x0004           ; interrupt vector location
;
;     <Search the device datasheet for 'context' and copy interrupt
;     context saving code here.  Older devices need context saving code,
;     but newer devices like the 16F#### don't need context saving code.>
;
;     RETFIE
;
;----------------------------------PIC18's--------------------------------------
;
; ISRHV     CODE    0x0008
;     GOTO    HIGH_ISR
; ISRLV     CODE    0x0018
;     GOTO    LOW_ISR
;
; ISRH      CODE                     ; let linker place high ISR routine
; HIGH_ISR
;     <Insert High Priority ISR Here - no SW context saving>
;     RETFIE  FAST
;
; ISRL      CODE                     ; let linker place low ISR routine
; LOW_ISR
;       <Search the device datasheet for 'context' and copy interrupt
;       context saving code here>
;     RETFIE
;
;*******************************************************************************
    BANKSEL LATA
    BCF LATA, Debug_Bit2
    
    ; Start Transmit ISR
    ; Check if interrupt is active, end Transmit ISR if interrupt is active
    BANKSEL PIR0
    BTFSS PIR0, 5
    GOTO TransmitISREnd
   
    ; Start Transmit
    ; Clear sourse of interrupt
    BANKSEL PIR0
    BCF PIR0, 5
        
    ; Decrement the counter
    BANKSEL TransmitCounter
    DECF TransmitCounter, F
    ; If the status bit Z is 0, skip to TransmitMessageFinished
    BTFSC STATUS, Z
    GOTO TransmitMessageFinished
    ; The counter is greater than 0 so there is message remaining
    ; Send the LSB of TransmitByte to the transmit port
    BANKSEL LATA
    BTFSC TransmitByte, 0
    BSF LATA, Transmit_Bit
    BTFSS TransmitByte, 0
    BCF LATA, Transmit_Bit
    ; Shift TransmitByte 1 position right
    RRF TransmitByte, F
    ; Set MSB of TransmitByte to create the stop bit
    BSF TransmitByte, 7
        
    GOTO TransmitISREnd
    ; If TransmitCounter is  0
TransmitMessageFinished:
    ; Disable the interrupt
    BANKSEL PIE0
    BCF PIE0, 5
    ; Stop the timer
    BANKSEL T0CON0
    BCF T0CON0, 7
    ; Set Transmit to inactive
    BANKSEL TransmitActive
    BCF TransmitActive, 0
TransmitISREnd:
    
    ; Start Recieve IOC ISR
    ; Check if interrupt is active, end Transmit ISR if interrupt is active
    BANKSEL PIR0
    BTFSS PIR0, 4
    GOTO ReceiveIOCISREnd
    
    ; If currently reading, go to end
    BANKSEL ReceiveActive
    BTFSC ReceiveActive, 0
    GOTO ReceiveIOCISREnd
    
    ; Start Receive
    ; Clear the source of the interrupt
    BANKSEL IOCAF
    BCF IOCAF, Receive_Bit
    
    ; Check value on the line is low, that this is the correct change
    BANKSEL PORTA
    BTFSC PORTA, Receive_Bit
    GOTO ReceiveIOCISREnd
    
    ; Start the timer to expire after 1/2 bit time (2400 baud)
    ; This is a value of 103
    BANKSEL T2PR
    MOVLW .103
    MOVWF T2PR
    
    ; Set the initial timer value to 0
    BANKSEL TMR2
    CLRF TMR2
    
    ; Set ReceiveStarting
    BANKSEL ReceiveStarting
    BSF ReceiveStarting, 0
    ; Set ReceiveCounter to 9
    BANKSEL ReceiveCounter
    MOVLW .9
    MOVWF ReceiveCounter
    
    BCF LATA, Debug_Bit
    GOTO $+1
    GOTO $+1
    GOTO $+1
    BSF LATA, Debug_Bit
    
    ; Disable interrupt on change
    BANKSEL PIE0
    BCF PIE0, 4
    ; Enable timer interrupts
    BANKSEL PIE4
    BSF PIE4, 1
    ; Start the timer
    BANKSEL T2CON
    BSF T2CON, 7
        
ReceiveIOCISREnd:
    
    ; Start Receive timer ISR
    ; Check if the interrupt is active, go to end otherwise
    BANKSEL PIR4
    BTFSS PIR4, 1
    GOTO ReceiveISREnd
    
    ; Clear the source of the interrupt
    BANKSEL PIR4
    BCF PIR4, 1
        
    ; If ReceiveStarting = 1, doing the start bit operations
    ; Otherwise, go to the message processing section
    BANKSEL ReceiveStarting
    BTFSS ReceiveStarting, 0
    GOTO ReceiveISRMessage
    
    ; Clear ReceiveStarting
    BANKSEL ReceiveStarting
    CLRF ReceiveStarting
    ; Check if receive is low to ensure we have a good start bit
    BANKSEL PORTA
    BTFSC PORTA, Receive_Bit
    GOTO ReceiveISRBad
    
    ; If the bit is not set, the start bit is valid
    ; Stop the timer
    BANKSEL T2CON
    BCF T2CON, 7
    ; Reset the timer
    BANKSEL TMR2
    CLRF TMR2
    ; Set the timer to expire at a 2400 baud rate
    ; This is a value of 206
    BANKSEL T2PR
    MOVLW .206
    MOVWF T2PR
    ; Start the timer
    BANKSEL T2CON
    BSF T2CON, 7
    
    ; Set ReceiveActive
    BANKSEL ReceiveActive
    BSF ReceiveActive, 0
       
    ; Exit the ISR
    GOTO ReceiveISREnd
    
    ; If the bit is set, the start bit is invalid    
ReceiveISRBad:
    ; Clear the IOC pending flags
    BANKSEL IOCAF
    BCF IOCAF, Receive_Bit
    ; Re-enable the interrupt on change for receive pin
    BANKSEL PIE0
    BSF PIE0, 4
    ; Disable timer interrupts
    BANKSEL PIE4
    BCF PIE4, 1
    ; Stop the timer
    BANKSEL T2CON
    BCF T2CON, 7
    ; Clear flags
    BANKSEL ReceiveActive
    BCF ReceiveActive, 0
        
    ; Exit ISR
    GOTO ReceiveISREnd    
    
ReceiveISRMessage:
    ; Add the current value read by the receive to the ReceiveByte
    ; Decrement ReceiveCounter
    BANKSEL ReceiveCounter
    DECF ReceiveCounter, F
        
    ; If the counter is not zero, add the current value to the ReceiveByte
    BTFSC STATUS, Z
    GOTO ReceiveMessageEnd
        
    ; Counter is not 0
    ; Shift ReceiveByte one position to the right
    BANKSEL ReceiveByte
    RRF ReceiveByte, F
    
    ; Copy the state of the Receive_Bit to the Receive Byte MSB    
    BANKSEL PORTA
    BTFSC PORTA, Receive_Bit
    BSF ReceiveByte, 7
    BTFSS PORTA, Receive_Bit
    BCF ReceiveByte, 7
        
    ; Exit the ISR
    GOTO ReceiveISREnd
    
ReceiveMessageEnd:
    ; The bit currently being read is the stop bit
    ; If the value is high, the data being received is good, mark ReceiveComplete
    BANKSEL ReceiveComplete
    BSF ReceiveComplete, 0
    ; If the value is not high, do not mark it as complete
    BANKSEL PORTA
    BTFSS PORTA, Receive_Bit
    BCF ReceiveComplete, 0
        
    ; Disable timer interrupts
    BANKSEL PIE4
    BCF PIE4, 1
    ; Stop the timer
    BANKSEL T2CON
    BCF T2CON, 7
    ; Re-enable the interrupt on change for receive pin
    ; Clear the IOC pending flags
    BANKSEL IOCAF
    BCF IOCAF, Receive_Bit
    BANKSEL PIE0
    BSF PIE0, 4
    
    ; Mark values
    BANKSEL ReceiveActive
    CLRF ReceiveActive
    
ReceiveISREnd: 
    
    BANKSEL LATA
    BSF LATA, Debug_Bit2
    ; Exit interrupt
    RETFIE

;*******************************************************************************
; MAIN PROGRAM
;*******************************************************************************

MAIN_PROG CODE                      ; let linker place main program
 
InitiateWrite:
    ; If there is a transmit currently active, do not write a new one    
    ; Check the TransmitActive register, write value if no active transmit
    BANKSEL TransmitActive
    BTFSC TransmitActive, 0
    GOTO Loop
    ; Clear the ReceiveComplete
    BANKSEL ReceiveComplete
    BCF ReceiveComplete, 0
    ; Write the current message from ReceiveByte using 2400 baud rate
    BANKSEL ReceiveByte
    MOVF ReceiveByte, W
    ; Convert from a lowercase letter to uppercase
    ADDLW -.32
    MOVWF TransmitByte
   
    ; Set transmit status as active
    MOVLW 1
    MOVWF TransmitActive
    ; Set the shift counter to 10
    MOVLW 0x0A
    MOVWF TransmitCounter
    ; Reset the timer value
    BANKSEL TMR0L
    CLRF TMR0L
    ; Start timer 0 to fire at 2400 baud
    BANKSEL T0CON0
    BSF T0CON0, 7
    ; Write start bit, lower transit line
    BANKSEL LATA
    BCF LATA, Transmit_Bit
    ; Clear pending interrupts and enable locally
    BANKSEL PIR0
    CLRF PIR0
    BANKSEL PIE0
    BSF PIE0, 5
    
    GOTO Loop
    
;Pulse the debug line
PulseDebug:
    BANKSEL BSR
    MOVF BSR, w ;save the back select register so that we can
    BANKSEL BSR_temp
    MOVWF BSR_temp ;call this routine from anywhere without worry
    BANKSEL LATA
    BCF LATA, Debug_Bit
    GOTO $+1
    GOTO $+1
    GOTO $+1
    BSF LATA, Debug_Bit
    BANKSEL BSR_temp
    MOVF BSR_temp, w ;restore the BSR to get the incoming bank back
    BANKSEL BSR
    MOVWF BSR
    GOTO Loop

; Hardware setup of PIC
Setup
    ; Initalize the instruction clock rate to 8 Mhz
    BANKSEL OSCFRQ
    BSF OSCFRQ, 0
    BSF OSCFRQ, 1
    BCF OSCFRQ, 2
    
    ; Initalize the transmit port A1 as digital output
    ; Initialize the receive port A0 as digital input
    ; Initialize debug port A2 as a digital output
    BANKSEL PORTA
    CLRF PORTA
    ; Set output latch value as 1 for transmit standby
    BANKSEL LATA
    BSF LATA, Transmit_Bit
    BSF LATA, Debug_Bit
    BSF LATA, Debug_Bit2
    ; Clear ANSEL register for digital I/O
    BANKSEL ANSELA
    CLRF ANSELA
    ; Set TRIS A1 register for output and A0 for input
    BANKSEL TRISA
    BCF TRISA, Transmit_Bit
    BSF TRISA, Receive_Bit
    BCF TRISA, Debug_Bit
    BCF TRISA, Debug_Bit2
    
    ; Initialize Timer 0 for interrupts at 2400 baud
    ; Set to 8 bit mode
    BANKSEL T0CON0
    BCF T0CON0, 4
    ; Set the clock sourse to Fosc/4
    BANKSEL T0CON1
    BSF T0CON1, 6
    ; Set the prescaler to 4
    BSF T0CON1, 1
    ; Set the compare value in TMR0H so the interrupt triggers at 2400 baud
    ; This is a value of 206
    BANKSEL TMR0H
    MOVLW .206
    MOVWF TMR0H
    
    ; Initialize Timer 2 for interruts at 2400 baud
    ; Already set to 8 bit mode
    ; Set the clock source fo Fosc/4
    BANKSEL T2CLKCON
    BSF T2CLKCON, 0
    ; Set the prescaler to 4
    BANKSEL T2CON
    BSF T2CON, 5
    ; Set the inital compare value of T2PR for 1/2 of 2400 baud
    ; This is a value of 103
    BANKSEL T2PR
    MOVLW .103
    MOVWF T2PR
    
    ; Setup interrupt on change for receive pin for falling edge (start bit)
    BANKSEL IOCAN
    BSF IOCAN, Receive_Bit
       
    ; Initialize TransmitActive as 0
    BANKSEL TransmitActive
    MOVLW 0
    MOVWF TransmitActive
    
    ; Initialize ReceiveActive as 0
    BANKSEL ReceiveActive
    CLRF ReceiveActive
    
    ; Initialize RecieveComplete as 0
    BANKSEL ReceiveComplete
    CLRF ReceiveComplete
    
    ; Initialize RecieveStarting as 0
    BANKSEL ReceiveStarting
    CLRF ReceiveStarting
    
    ; Clear flags for global interrupt
    BANKSEL PIR0
    CLRF PIR0
    BANKSEL PIR4
    CLRF PIR4
    
    ; Enable global interrupts
    BANKSEL INTCON
    BSF INTCON, 7
    BSF INTCON, 6
    
    ; Enable receive start bit detection
    BANKSEL PIE0
    BSF PIE0, 4
    
    ; Show that setup has occured
    BANKSEL LATA
    BCF LATA, Debug_Bit
    GOTO $+1
    GOTO $+1
    BSF LATA, Debug_Bit
    
    GOTO Loop
 
 
START
    GOTO Setup
    
Loop:
    
    ; If a message is finished being receive, write it
    BANKSEL ReceiveComplete
    BTFSC ReceiveComplete, 0
    GOTO InitiateWrite
        
    GOTO Loop                          ; loop forever

    END
   